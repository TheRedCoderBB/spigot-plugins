package trc.sl;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import com.google.gson.Gson;

import trc.sl.json.SpongeActionData;
import trc.sl.json.SpongeData;
import trc.sl.json.Vector3Data;
import trc.sl.utilities.MaterialUtilities;
import trc.sl.utilities.TRCommand;

public class Main extends JavaPlugin {
	
	public static Main instance;
	public static PluginDescriptionFile pdf;
	public static ConsoleCommandSender console;
	
	public static String NAME;
	public static String VERSION;
	
	@Override
	public void onEnable() {
		instance = this;
		pdf = getDescription();
		console = getServer().getConsoleSender();
		
		NAME = pdf.getName();
		VERSION = pdf.getVersion();
		
		initDataFile();
		loadConfig();
		registerCommands();
		registerListeners();
		
		startTicker();
		
		console.sendMessage("[" +  NAME + "] " + NAME + " v" + VERSION + " has been ENABLED!");
	}
	
	@Override
	public void onDisable() {
		console.sendMessage("[" + NAME + "] " + NAME + " v" + VERSION + " has been DISABLED!");
		saveDataFile();
	}
	
	@SuppressWarnings("deprecation")
	private void loadConfig() {
		saveDefaultConfig();
		List<String> allowedTypes = getConfig().getStringList("allowed-blocks");
		for(String type : allowedTypes) {
			Material m = Material.matchMaterial(type);
			if(m == null) m = Material.getMaterial(type);
			if(m == null) m = getServer().getUnsafe().getMaterialFromInternalName(type);
			if(m == null) m = MaterialUtilities.getMaterialFromId(type);
			if(m != null && !Data.allowed_types.contains(m)) Data.allowed_types.add(m);
		}
	}
	
	private void initDataFile() {
		if(!getDataFolder().exists()) getDataFolder().mkdir();
		
		try {
			File dataFile = new File(getDataFolder() + "/sponges.json");
			if(!dataFile.exists())
				dataFile.createNewFile();
			
			String json = new String(Files.readAllBytes(dataFile.toPath()));
			Gson gson = new Gson();
			SpongeData[] spongeDataArray = gson.fromJson(json, SpongeData[].class);
			List<SpongeData> spongeData = spongeDataArray == null ? new ArrayList<SpongeData>() : Arrays.asList(spongeDataArray);
			for(SpongeData sd : spongeData) {
				Location location = new Location(getServer().getWorld(sd.getWorld()),
						sd.getLocation().getX(), sd.getLocation().getY(), sd.getLocation().getZ());
				List<SpongeAction> actions = new ArrayList<SpongeAction>();
				for(SpongeActionData sad : sd.getActions()) {
					Vector velocity = new Vector(sad.getVelocity().getX(), sad.getVelocity().getY(), sad.getVelocity().getZ());
					actions.add(new SpongeAction(velocity, sad.getDuration()));
				}
				boolean stop = sd.isStop();
				Sponge sponge = new Sponge(location, actions, stop);
				Data.sponges.add(sponge);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveDataFile() {
		if(!getDataFolder().exists()) getDataFolder().mkdir();
		
		try {
			File dataFile = new File(getDataFolder() + "/sponges.json");
			if(!dataFile.exists())
				dataFile.createNewFile();
			
			Gson gson = new Gson();
			List<SpongeData> spongeData = new ArrayList<SpongeData>();
			for(Sponge s : Data.sponges) {
				Vector3Data location = new Vector3Data(s.getLocation().getBlockX(), s.getLocation().getBlockY(), s.getLocation().getBlockZ());
				List<SpongeActionData> actions = new ArrayList<SpongeActionData>();
				for(SpongeAction sa : s.getActions()) {
					Vector3Data velocity = new Vector3Data(sa.getVelocity().getX(), sa.getVelocity().getY(), sa.getVelocity().getZ());
					actions.add(new SpongeActionData(velocity, sa.getDuration()));
				}
				SpongeData sponge = new SpongeData(location, s.getLocation().getWorld().getName(), actions, s.isStop());
				spongeData.add(sponge);
			}
			String json = gson.toJson(spongeData);
			FileOutputStream fos = new FileOutputStream(dataFile, false);
			fos.write(json.getBytes());
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private <T extends TRCommand> void registerCommand(String command, T handler) {
		getCommand(command).setExecutor(handler);
		getCommand(command).setTabCompleter(handler);
	}
	
	private void registerCommands() {
		registerCommand("sponge", new SpongeCommand());
	}
	
	private void registerListeners() {
		getServer().getPluginManager().registerEvents(new SpongeListener(), this);
	}
	
	private int ticks = 0;
	private void startTicker() {
		getServer().getScheduler().scheduleSyncRepeatingTask(this,
				() -> { getServer().getPluginManager().callEvent(new TickEvent(++ticks)); },
		1L, 1L);
	}
}
