package trc.sl.json;

import java.util.List;

public class SpongeData {

	private Vector3Data location;
	private String world;
	private List<SpongeActionData> actions;
	private boolean stop;
	
	public SpongeData(Vector3Data location, String world, List<SpongeActionData> actions, boolean stop) {
		this.location = location;
		this.world = world;
		this.actions = actions;
		this.stop = stop;
	}
	
	public Vector3Data getLocation() {
		return location;
	}
	
	public String getWorld() {
		return world;
	}
	
	public List<SpongeActionData> getActions() {
		return actions;
	}
	
	public boolean isStop() {
		return stop;
	}
	
	public void setLocation(Vector3Data location) {
		this.location = location;
	}
	
	public void setWorld(String world) {
		this.world = world;
	}
	
	public void setActions(List<SpongeActionData> actions) {
		this.actions = actions;
	}
	
	public void setStop(boolean stop) {
		this.stop = stop;
	}
}
