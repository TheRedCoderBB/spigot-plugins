package trc.sl.json;

public class SpongeActionData {

	private Vector3Data velocity;
	private long duration;
	
	/**
	 * @param velocity
	 * @param duration Duration in ticks (1 tick = 1/20 seconds)
	 */
	public SpongeActionData(Vector3Data velocity, long duration) {
		this.velocity = velocity;
		this.duration = duration;
	}
	
	public Vector3Data getVelocity() {
		return velocity;
	}
	
	public long getDuration() {
		return duration;
	}
	
	public void setVelocity(Vector3Data velocity) {
		this.velocity = velocity;
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
	}
}
