package trc.sl;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;

public class Data {
	
	public static List<Sponge> sponges;
	public static List<Material> allowed_types;
	
	public static void addSponge(Sponge sponge) {
		sponges.add(sponge);
		Main.instance.saveDataFile();
	}
	
	public static void removeSponge(Sponge sponge) {
		sponges.remove(sponge);
		Main.instance.saveDataFile();
	}
	
	static {
		sponges = new ArrayList<Sponge>();
		allowed_types = new ArrayList<Material>();
	}
}
