package trc.sl;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TickEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	
	private int tick;
	
	public TickEvent(int tick) {
		this.tick = tick;
	}
	
	public int getTick() {
		return tick;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
        return handlers;
    }
}
