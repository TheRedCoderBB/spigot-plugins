package trc.sl;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

public class Sponge {

	private Location location;
	private List<SpongeAction> actions;
	private boolean stop;
	
	public Sponge(Location location, List<SpongeAction> actions, boolean stop) {
		this.location = location;
		this.actions = actions;
		this.stop = stop;
	}
	
	public Sponge(Location location) {
		this(location, new ArrayList<SpongeAction>(), true);
	}
	
	public Location getLocation() {
		return location;
	}
	
	public List<SpongeAction> getActions() {
		return actions;
	}
	
	public boolean isStop() {
		return stop;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public void setActions(List<SpongeAction> actions) {
		this.actions = actions;
	}
	
	public void setStop(boolean stop) {
		this.stop = stop;
	}
}
