package trc.sl;

import static trc.sl.utilities.StringUtilities.equalsIgnoreCase;
import static trc.sl.utilities.StringUtilities.pad;
import static trc.sl.utilities.TextComponents.commandComponent;
import static trc.sl.utilities.TextComponents.errorComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import trc.sl.utilities.TRCommand;
import trc.sl.utilities.StringUtilities.PadLocation;

public class SpongeCommand implements TRCommand {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("You need to be a player to do that!");
			return true;
		}
		Player player = (Player) sender;
		if(!player.isOp()) {
			player.spigot().sendMessage(errorComponent(new TextComponent("You don't have the permission to do that!")));
			return true;
		}
		if(args.length == 0 || (args.length > 0 && equalsIgnoreCase(args[0], "help", "?", "-h")))
			sendHelp(player);
		else if(args.length > 0) {
			if(equalsIgnoreCase(args[0], "info", "details", "-i"))
				sendInfo(player);
			else if(equalsIgnoreCase(args[0], "add", "create", "new", "-a"))
				addSponge(player);
			else if(equalsIgnoreCase(args[0], "remove", "rem", "delete", "del", "-d"))
				removeSponge(player);
			else if(args.length > 1 && equalsIgnoreCase(args[0], "edit", "-e"))
				editSponge(player, args);
			else
				sendHelp(player);
		} else
			sendHelp(player);
		return true;
	}
	
	private void sendHelp(Player player) {
		TextComponent header = new TextComponent(new ComponentBuilder("")
				.append("  --[ ")			.color(ChatColor.GREEN)		.bold(true)
				.append("Sponge Launchers")	.color(ChatColor.YELLOW)	.bold(true)
				.append(" ]--  ")			.color(ChatColor.GREEN)		.bold(true)
				.create());
		
		//     <--[ Sponge Launchers ]-->
		
		TextComponent lineBreak = new TextComponent("\n");

		TextComponent help = commandComponent("help", "Displays this message", "?", "-h");
		TextComponent info = commandComponent("info", "Displays information about the launcher you are standing on", "details", "-i");
		TextComponent add = commandComponent("add", "Make the block you are standing on a new launcher", "create", "new", "-a");
		TextComponent del = commandComponent("remove", "Removes the launcher you are standing on", "rem", "delete", "del", "-d");
		TextComponent edit = commandComponent("edit", "Edit the behavior of the launcher you are standing on", "-e");
		
		player.spigot().sendMessage(		lineBreak,
									header,	lineBreak,
									help,	lineBreak,
									info,	lineBreak,
									add,	lineBreak,
									del,	lineBreak,
									edit,	lineBreak);
	}
	
	private void sendInfo(Player player) {
		Block block = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
		Sponge sponge = null;
		for(Sponge s : Data.sponges)
			if(s.getLocation().getBlock().equals(block)) {
				sponge = s;
				break;
			}
		if(sponge == null) {
			player.spigot().sendMessage(errorComponent(new TextComponent("This block is not a launcher!")));
			return;
		}
		TextComponent header = new TextComponent(new ComponentBuilder("")
				.append("  --[ ")			.color(ChatColor.GREEN)		.bold(true)
				.append("Sponge Launchers")	.color(ChatColor.YELLOW)	.bold(true)
				.append(" ]--  ")			.color(ChatColor.GREEN)		.bold(true)
				.create());
		String x = pad("" + sponge.getLocation().getBlockX(), PadLocation.AFTER, ' ', 10);
		String y = pad("" + sponge.getLocation().getBlockY(), PadLocation.AFTER, ' ', 10);
		String z = pad("" + sponge.getLocation().getBlockZ(), PadLocation.AFTER, ' ', 10);
		TextComponent cordinates = new TextComponent(new ComponentBuilder("")
				.append("    X: ")	.color(ChatColor.GREEN)
				.append(x)			.color(ChatColor.AQUA)
				.append(" Y: ")		.color(ChatColor.GREEN)
				.append(y)			.color(ChatColor.AQUA)
				.append(" Z: ")		.color(ChatColor.GREEN)
				.append(z)			.color(ChatColor.AQUA)
				.create());
		TextComponent actions = new TextComponent(new ComponentBuilder("")
				.append("Actions:").color(ChatColor.GREEN).bold(true)
				.create());
		for(SpongeAction sa : sponge.getActions()) {
			String vx = pad(sa.getVelocity().getX() + "", PadLocation.AFTER, ' ', 10);
			String vy = pad(sa.getVelocity().getY() + "", PadLocation.AFTER, ' ', 10);
			String vz = pad(sa.getVelocity().getZ() + "", PadLocation.AFTER, ' ', 10);
			String dur = pad(sa.getDuration() + " ticks", PadLocation.AFTER, ' ', 10 + 6);
			TextComponent action = new TextComponent(new ComponentBuilder(" - ").color(ChatColor.GRAY)
					.append(" X: ")			.color(ChatColor.GREEN)
					.append(vx)				.color(ChatColor.AQUA)
					.append(" Y: ")			.color(ChatColor.GREEN)
					.append(vy)				.color(ChatColor.AQUA)
					.append(" Z: ")			.color(ChatColor.GREEN)
					.append(vz)				.color(ChatColor.AQUA)
					.append(" Duration: ")	.color(ChatColor.GREEN)
					.append(dur)			.color(ChatColor.AQUA)
					.create());
			actions.addExtra("\n");
			actions.addExtra(action);
		}
		TextComponent message = new TextComponent();
		message.addExtra("\n");
		message.addExtra(header);
		message.addExtra("\n");
		message.addExtra(cordinates);
		message.addExtra("\n");
		message.addExtra(actions);
		player.spigot().sendMessage(message);
	}
	
	private void addSponge(Player player) {
		Block block = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
		for(Sponge s : Data.sponges)
			if(s.getLocation().getBlock().equals(block)) {
				player.spigot().sendMessage(errorComponent(new TextComponent("This block is already a launcher!")));
				return;
			}
		if(block == null || block.getType() == Material.AIR || !Data.allowed_types.contains(block.getType()) ) {
			player.spigot().sendMessage(errorComponent(new TextComponent("This block's type is not a valid launcher type!")));
			return;
		}
		Sponge sponge = new Sponge(block.getLocation());
		sponge.getActions().add(new SpongeAction(new Vector(0, 1, 0), 20L));
		Data.addSponge(sponge);
		player.spigot().sendMessage(new ComponentBuilder("Launcher added!").color(ChatColor.GREEN).create());
	}
	
	private void removeSponge(Player player) {
		Block block = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
		Sponge remove = null;
		for(Sponge s : Data.sponges)
			if(s.getLocation().getBlock().equals(block)) {
				remove = s;
				break;
			}
		if(remove == null) {
			player.spigot().sendMessage(errorComponent(new TextComponent("This block is not a launcher!")));
			return;
		}
		Data.removeSponge(remove);
		player.spigot().sendMessage(new ComponentBuilder("Launcher removed!").color(ChatColor.GREEN).create());
	}
	
	private void editSponge(Player player, String[] args) {
		if(args.length < 1 || (args.length == 1 && !args[0].equalsIgnoreCase("edit"))) {
			sendInfo(player);
			return;
		}
		args = Arrays.copyOfRange(args, 2, args.length);
		Main.instance.console.sendMessage(args[0]);
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) return null;
		if(!((Player)sender).isOp()) return null;
		if(args.length == 1) {
			String[] subCommandsArray = { "help", "info", "add", "remove", "edit" };
			String[] flagsArray = { "-h", "-i", "-a", "-d", "-e" };
			
			List<String> rtrn = new ArrayList<String>();
			
			String param = args[0];
			if(param.startsWith("-")) {
				for(String s : flagsArray) if(s.toLowerCase().startsWith(param.toLowerCase())) rtrn.add(s);
			} else
				for(String s : subCommandsArray) if(s.toLowerCase().startsWith(param.toLowerCase())) rtrn.add(s);
			return rtrn;
		} else if(args.length == 2 && args[0].equalsIgnoreCase("edit")) {
			String[] subCommandsArray = { "help", "add", "remove" };
			String[] flagsArray = { "-h", "-a", "-d" };
			
			List<String> rtrn = new ArrayList<String>();
			
			String param = args[1];
			if(param.startsWith("-")) {
				for(String s : flagsArray) if(s.toLowerCase().startsWith(param.toLowerCase())) rtrn.add(s);
			} else
				for(String s : subCommandsArray) if(s.toLowerCase().startsWith(param.toLowerCase())) rtrn.add(s);
			return rtrn;
		}
		return null;
	}
}
