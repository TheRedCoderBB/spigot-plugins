package trc.sl;

import org.bukkit.util.Vector;

public class SpongeAction {

	private Vector velocity;
	private long duration;
	
	public SpongeAction(Vector velocity, long duration) {
		this.velocity = velocity;
		this.duration = duration;
	}
	
	public SpongeAction(SpongeAction spongeAction) {
		this(spongeAction.velocity, spongeAction.duration);
	}

	public Vector getVelocity() {
		return velocity;
	}
	
	public long getDuration() {
		return duration;
	}
	
	public void setVelocity(Vector velocity) {
		this.velocity = velocity;
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
	}
}
