package trc.sl.utilities;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabCompleter;

public interface TRCommand extends CommandExecutor, TabCompleter {}
