package trc.sl.utilities;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_12_R1.Item;
import net.minecraft.server.v1_12_R1.MinecraftKey;

public class MaterialUtilities {

	public static Material getMaterialFromId(String id) {
		MinecraftKey mk = new MinecraftKey(id);
		ItemStack item = CraftItemStack.asNewCraftStack(Item.REGISTRY.get(mk));
		return item.getType();
	}
}
