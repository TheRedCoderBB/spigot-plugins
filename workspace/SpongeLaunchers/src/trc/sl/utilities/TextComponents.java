package trc.sl.utilities;

import java.util.Arrays;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class TextComponents {

	public static TextComponent errorComponent(TextComponent error) {
		TextComponent err = new TextComponent(
				new ComponentBuilder("")
				.append("Error: ").color(ChatColor.DARK_RED).bold(true)
				.create());
		TextComponent e = new TextComponent(error);
		e.setColor(ChatColor.RED);
		err.addExtra(e);
		return err;
	}
	
	public static TextComponent commandComponent(String subCommand, String explenation, String... aliases) {
		BaseComponent[] hover = new ComponentBuilder("Try this command!").color(ChatColor.WHITE).create();
		
		TextComponent prefix = new TextComponent(" /sponge ");
		prefix.setColor(ChatColor.WHITE);
		TextComponent explenationTick = new TextComponent(" - ");
		explenationTick.setColor(ChatColor.GRAY);
		
		TextComponent command = new TextComponent(subCommand);
		command.setColor(ChatColor.AQUA);
		command.setBold(true);
		TextComponent commandExplenation = new TextComponent(explenation + " ");
		commandExplenation.setColor(ChatColor.WHITE);

		TextComponent commandAliases = new TextComponent(Arrays.toString(aliases));
		commandAliases.setColor(ChatColor.DARK_GRAY);
		commandAliases.setItalic(true);
		TextComponent commandComponent = new TextComponent(prefix, command, explenationTick, commandExplenation, commandAliases);
		commandComponent.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/sponge " + subCommand));
		commandComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hover));
		return commandComponent;
	}
}
