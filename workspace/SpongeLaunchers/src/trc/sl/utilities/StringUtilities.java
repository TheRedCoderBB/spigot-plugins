package trc.sl.utilities;

public class StringUtilities {

	public static boolean equalsIgnoreCase(String a, String... strings) {
		try {
			for(String s : strings) if(a.equalsIgnoreCase(s)) return true;			
		} catch(Exception e) {}
		return false;
	}
	
	public static String pad(String str, PadLocation padLocation, char padding, int minLength) {
		String rtrn = str;
		while(rtrn.length() < minLength)
			rtrn = padLocation == PadLocation.BEFORE ? padding + rtrn : rtrn + padding;
		return rtrn;
	}
	
	public enum PadLocation {
		BEFORE, AFTER
	}
}
