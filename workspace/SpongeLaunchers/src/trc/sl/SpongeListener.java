package trc.sl;

import static trc.sl.utilities.TextComponents.errorComponent;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.UUID;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;

public class SpongeListener implements Listener {

	private Map<UUID, Map.Entry<Boolean, Stack<SpongeAction>>> launching = new HashMap<UUID, Map.Entry<Boolean, Stack<SpongeAction>>>();
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Block block = event.getTo().getBlock().getRelative(BlockFace.DOWN);
		if(player.isSneaking()) return;
		if(launching.containsKey(player.getUniqueId())) return;
		if(!Data.allowed_types.contains(block.getType())) return;
		Sponge sponge = null;
		for(Sponge s : Data.sponges)
			if(s.getLocation().getBlock().equals(block)) {
				sponge = s;
				break;
			}
		if(sponge == null) return;
		Stack<SpongeAction> launch = new Stack<SpongeAction>();
		for(int i = sponge.getActions().size() - 1; i >= 0; i--)
			launch.push(new SpongeAction(sponge.getActions().get(i)));
		Map.Entry<Boolean, Stack<SpongeAction>> pair = new AbstractMap.SimpleEntry<Boolean, Stack<SpongeAction>>(sponge.isStop(), launch);
		launching.put(player.getUniqueId(), pair);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		Sponge sponge = null;
		for(Sponge s : Data.sponges)
			if(s.getLocation().getBlock().equals(block)) {
				sponge = s;
				break;
			}
		if(sponge == null) return;
		if(event.isCancelled()) return;
		if(!player.isOp()) {
			event.setCancelled(true);
			player.spigot().sendMessage(errorComponent(new TextComponent("You don't have the permission to do that!")));
			return;
		}
		Data.removeSponge(sponge);
		TextComponent message = new TextComponent(new ComponentBuilder("Launcher removed!").color(ChatColor.GREEN).create());
		player.spigot().sendMessage(message);
	}
	
	@EventHandler
	public void onTick(TickEvent event) {
		List<UUID> toRemove = new ArrayList<UUID>();
		for(Entry<UUID, Map.Entry<Boolean, Stack<SpongeAction>>> entry : launching.entrySet()) {
			Player player = Main.instance.getServer().getPlayer(entry.getKey());
			Map.Entry<Boolean, Stack<SpongeAction>> pair = entry.getValue();
			Stack<SpongeAction> actions = pair.getValue();
			if(actions.isEmpty()) {
				toRemove.add(entry.getKey());
				if(pair.getKey()) player.setVelocity(new Vector(0, 0, 0));
				player.setFallDistance(-100000);
				continue;
			}
			player.setVelocity(actions.peek().getVelocity());
			player.setFallDistance(-100000);
			actions.peek().setDuration(actions.peek().getDuration() - 1L);
			if(actions.peek().getDuration() <= 0L) actions.pop();
		}
		for(UUID uuid : toRemove) launching.remove(uuid);
	}
}
